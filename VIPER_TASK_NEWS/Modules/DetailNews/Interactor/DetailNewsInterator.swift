//
//  DetailNewsInterator.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/13/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import Foundation

class DetailNewsInteractor{
    
    weak var detailNewsPresenter: DetailNewsPresenterOutputProtocol?
    
    var newsModel: NewsModel?
    
    init(news: NewsModel){
        self.newsModel = news
    }
    
}

extension DetailNewsInteractor: DetailNewsInteractorInputProtocol{
    
    func getDataNews(){
        
        let item = self.newsModel
        detailNewsPresenter?.setDataNews(news: item!)
        
    }
    
}
