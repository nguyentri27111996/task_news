//
//  DetailNewsPresenter.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/13/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import Foundation

class DetailNewsPresenter{
    
    var detailNewsInteractor: DetailNewsInteractorInputProtocol?
    weak var detailNewsView: DetailNewsViewProtocol?
    
}

extension DetailNewsPresenter: DetailNewsPresenterInputProtocol{
    
    func getDataNews() {
        detailNewsInteractor?.getDataNews()
    }
    
}

extension DetailNewsPresenter: DetailNewsPresenterOutputProtocol{
    
    func setDataNews(news: NewsModel) {
        detailNewsView?.showDataNews(news: news)
    }
    
}
