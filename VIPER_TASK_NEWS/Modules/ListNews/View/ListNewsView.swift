//
//  ListNewsView.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/12/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import UIKit

class ListNewsView: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    
    var listNews: [NewsModel] = []
    
    var listNewsPrensenter: ListNewsPresenterInputProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listNewsPrensenter?.getDataListNews()
    }
    deinit {
        print("deinit ListNewsView")
    }
}

extension ListNewsView: ListNewsViewProtocol{
    
    func showDataListNews(listNews: [NewsModel]) {
        self.listNews = listNews
        tblView.separatorStyle = .none
        tblView.reloadData()
    }
}

extension ListNewsView: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "cellListNews", for: indexPath) as! CellListNewsView
        
        let newsModel = listNews[indexPath.row]
        
        cell.imgView.image = UIImage(named: newsModel.getImage())
        cell.titleView.text = newsModel.getTitle()
        cell.timeView.text = newsModel.getTime()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = listNews[indexPath.row]
        listNewsPrensenter?.showDetailNews(news: item)
    }
    
}


