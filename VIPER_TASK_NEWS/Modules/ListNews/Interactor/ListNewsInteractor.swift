//
//  ListNewsInteractor.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/12/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import Foundation

class ListNewsInteractor{
    
    var listNews: [NewsModel] = []
    weak var listNewsPresenter: ListNewsPresenterOutputProtocol?

}

extension ListNewsInteractor: ListNewsInteractorInput{
    
    func getDataListNews(){
        // read JSON File
        // add to listNews
        // setData for listNewsPresenter
        if let path = Bundle.main.path(forResource: "News", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let list = jsonResult["ListNews"] as? [NSDictionary] {
                    // do stuff
                    
                    for item in list{
                        let newsModel = NewsModel(item: item)
                        listNews.append(newsModel)
                    }
                    
                    // INTERACTOR --> PRESENTER
                    listNewsPresenter?.setDataListNews(listNews: listNews)
                    
                    
                    
                }
            } catch {
                // handle error
            }
        }
    }
    
}

