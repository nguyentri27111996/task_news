//
//  NewsModel.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/12/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import Foundation

class NewsModel{
    private var title: String?
    private var time: String?
    private var head: String?
    private var content: String?
    private var image: String?
    
    init(title: String, time: String, head: String, content: String, image: String){
        self.title = title
        self.time = time
        self.head = head
        self.content = content
        self.image = image
    }
    
    init(item: NSDictionary){
        
        self.title = item.value(forKey: "title") as? String
        self.time = item.value(forKey: "time") as? String
        self.head = item.value(forKey: "head") as? String
        self.content = item.value(forKey: "content") as? String
        self.image = item.value(forKey: "image") as? String
        
    }
    
    func getTitle()->String{
        return self.title!
    }
    
    func getTime()->String{
        return self.time!
    }
    
    func getHead()->String{
        return self.head!
    }
    
    func getContent()->String{
        return self.content!
    }
    
    func getImage()->String{
        return self.image!
    }
    
    
}
