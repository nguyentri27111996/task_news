//
//  ListNewsPresenter.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/12/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import Foundation

class ListNewsPresenter{
    
    var listNewsInteractor: ListNewsInteractorInput?
    
    weak var listNewsView: ListNewsViewProtocol?
    
    var wireFrame: ListNewsWireframeProtocol?
    
}

extension ListNewsPresenter: ListNewsPresenterInputProtocol{
    
    func getDataListNews() {
        listNewsInteractor?.getDataListNews()
    }
    
    func showDetailNews(news: NewsModel) {
        wireFrame?.showDetailNewsView(fromView: listNewsView!, news: news)
    }
    
    
    
    
}

extension ListNewsPresenter: ListNewsPresenterOutputProtocol{
    
    func setDataListNews(listNews: [NewsModel]) {
        listNewsView?.showDataListNews(listNews: listNews)
    }
    
}
