//
//  AppDelegateTests.swift
//  VIPER_TASK_NEWSTests
//
//  Created by Nguyen Minh Tri on 12/21/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import XCTest
import UIKit
@testable import VIPER_TASK_NEWS

class AppDelegateTests: XCTestCase {
    
    var appDelegate: AppDelegate?
    var window: UIWindow? = UIWindow()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        appDelegate = AppDelegate()
        appDelegate?.baseWireframe = BaseWireFrame()
        appDelegate?.window = window
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitializeScreen(){
        
        let mainView = appDelegate?.baseWireframe?.createMainModule()
        XCTAssertNotNil(mainView, "TABBAR CONTROLLER DID NOT LOAD")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
