//
//  ListNewsInteractorTests.swift
//  VIPER_TASK_NEWSTests
//
//  Created by Nguyen Minh Tri on 12/21/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import XCTest
@testable import VIPER_TASK_NEWS

class ListNewsInteractorTests: XCTestCase {
    
    var listNewsInteractor: ListNewsInteractor?
    
    
    override func setUp() {
        super.setUp()
        listNewsInteractor = ListNewsInteractor()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLoadDataListNews(){
        listNewsInteractor?.getDataListNews()
        
        XCTAssertNotNil(listNewsInteractor?.listNews, "DATA DID NOT LOAD SUCCESSFULLY")
        XCTAssert(listNewsInteractor?.listNews.count == 6)
        
        let title = "New York - nỗi ám ảnh khôn nguôi về những kẻ khủng bố"
        XCTAssertEqual(title, listNewsInteractor?.listNews[0].getTitle(), "DID NOT GET DATA")
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
